
exports.up = function (knex, Promise) {
    return knex.schema
        .createTable('user', function (table) {
            table.increments('id').primary();
            table.string('username');
            table.string('password');
            table.string('type');
        })
        .createTable('quiz', function (table) {
            table.increments('id').primary();
            table.string('quizName');
            table.string('quizPassword');
            table.datetime('AvailableFrom');
            table.datetime('AvailableTo');
            table.integer('duration');
            table.integer('user_id', 10).unsigned().notNullable().references('id').inTable('user');
        })
        .createTable('questions', function (table) {
            table.increments('id').primary();
            table.string('questionText');
            table.bool('multipleAnwser');
            table.string('helpallowed');
            table.integer('anwserMin');
            table.integer('anwserMax');
            table.string('answers');
            table.string('hint');
            table.integer('quiz_id').unsigned().notNullable().references('id').inTable('quiz');
        })
        .createTable('response', function (table) {
            table.increments('id').primary();
            table.datetime('responseStart');
            table.datetime('responseEnd');
            table.integer('score');
            table.integer('user_id').unsigned().notNullable().references('id').inTable('user');
            table.integer('quiz_id').unsigned().notNullable().references('id').inTable('quiz');
        })
        .createTable('answer', function (table) {
            table.increments('id').primary();
            table.integer('selectedAnswer');
            table.integer('questions_id').unsigned().notNullable().references('id').inTable('questions');
            table.integer('response_id').unsigned().notNullable().references('id').inTable('response');
        })
};

exports.down = function (knex, Promise) {
    return knex.schema
        .table('quiz', function (table) {
            table.dropForeign('user_id');
            table.dropColumn('user_id');
        })
        .table('questions', function (table) {
            table.dropForeign('quiz_id');
            table.dropColumn('quiz_id');
        })
        .table('answer', function (table) {
            table.dropForeign('questions_id');
            table.dropColumn('questions_id');
            table.dropForeign('response_id');
            table.dropColumn('response_id');
        })
        .table('response', function (table) {
            table.dropForeign('user_id');
            table.dropColumn('user_id');
            table.dropForeign('quiz_id');
            table.dropColumn('quiz_id');
        })
        .dropTable('user')
        .dropTable('quiz')
        .dropTable('questions')
        .dropTable('answer')
        .dropTable('response');
};