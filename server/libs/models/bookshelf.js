let config = require('../../knexfile').development,
  knex = require('knex')(config),
  bookshelf = require('bookshelf')(knex);


let User = bookshelf.Model.extend({
  tableName: 'user',
  idAttribute: 'id',
  quiz: function () {
    return this.hasMany(Quiz);
  },
  response: function () {
    return this.hasMany(Response);
  }
});

let Quiz = bookshelf.Model.extend({
  tableName: 'quiz',
  idAttribute: 'id',
  user: function () {
    return this.belongsTo(User)
  },
  question: function () {
    return this.hasMany(Question);
  },
  response: function () {
    return this.hasMany(Response);
  }
});

let Question = bookshelf.Model.extend({
  tableName: 'questions',
  idAttribute: 'id',
  quiz: function () {
    return this.belongsTo(Quiz);
  },
  answer: function () {
    return this.hasMany(Answer);
  }
});

let Answer = bookshelf.Model.extend({
  tableName: 'answer',
  idAttribute: 'id',
  question: function () {
    return this.belongsTo(Question);
  },
  response: function () {
    return this.belongsTo(Response);
  }
});

let Response = bookshelf.Model.extend({
  tableName: 'response',
  idAttribute: 'id',
  user: function () {
    return this.belongsTo(User)
  },
  quiz: function () {
    return this.belongsTo(Quiz);
  },
  answer: function () {
    return this.hasMany(Answer);
  }
});

module.exports = {
  User,
  Quiz,
  Question,
  Answer,
  Response
}