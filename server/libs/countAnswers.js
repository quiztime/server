module.exports = {
    countAnswers: function (data, countParameter) {
        let groups = data.map(function (value, index) {
            return value[countParameter]
        });
        let group_count = {};
        groups.forEach(function (value, index) {
            if (value in group_count) {
                group_count[value] += 1;
            } else {
                group_count[value] = 1;
            }
        });
        return group_count;
    }
}