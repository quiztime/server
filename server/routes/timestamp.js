var express = require('express');
var router = express.Router();
var moment = require('moment');

/* GET users listing. Use: ?dataField=something&dataField2=something ...*/
router.get('/', function (req, res, next) {
      res.json({timestamp: moment(Date.now())});
});


module.exports = router;