var express = require('express');
var router = express.Router();

let Response = require('../libs/models/bookshelf').Response;
let Quiz = require('../libs/models/bookshelf').Quiz;

/* GET all responses */
router.get('/', function (req, res, next) {
  Response.fetchAll().then((result) => {
    res.json(result);
  });
});

/* GET all responses of user and quiz. */
router.post('/userQuiz', function (req, res, next) {
  Response.where('user_id', req.body.user_id).where('quiz_id', req.body.quiz_id).fetchAll().then((result) => {
    res.json(result);
  });
});

/* GET all responses of user. */
router.get('/user/:id', function (req, res, next) {
  Response.where('user_id', req.params.id).fetchAll().then((result) => {
    res.json(result);
  });
});

/* GET all responses of quiz. */
router.get('/quiz/:id', function (req, res, next) {
  Response.where('quiz_id', req.params.id).fetchAll().then((result) => {
    res.json(result);
  });
});

//POST => add response
router.post('/', (req, res, next) => {
  new Response().save(req.body).then((result) => {
    res.json(result);
  });
});

// PUT => update response
router.put('/:id', function (req, res, next) {
  new Response({ 'id': req.params.id }).save(req.body).then((result) => {
    res.json(result);
  });
});

// DELETE => OK
router.delete('/:id', function (req, res) {
  new Response({ 'id': req.params.id }).destroy().then((result) => {
    res.json(result)
  });
});

//posodobi response z izračunanim rezultatom tole ne dela 
router.put('/calculate/:id', function (req, res) {
  Quiz.where('id', req.params.id).fetchAll({
    withRelated: ['question']
  }).then((quiz) => {
    let odgovori = req.body.odgovori;

    console.log(odgovori);

    //prototip http://jsbin.com/wokezuzofu/1/edit?js,console
    var maxscore = 0
    var score = 0;
    var odg = [];

    //ustvarimo si seznam pravilnih odgovorov
    quiz.question.forEach(q => {
      let zac = [];
      q.answers.forEach(a => {
        if (a.valid === true) {
          maxscore++;
          zac.push(a.anwserID)
        }
      })
      odg.push({ "qestionID": q.id, "validAnwsers": zac })
    })
    //za lažje iskanje zmapiramo v mapico
    var mapica = odg.map(function (d) { return d['qestionID']; })

    odgovori.forEach(x => {
      let zacasniOdgovor = odg[mapica.indexOf(x.questions_id)];

      if (zacasniOdgovor.validAnwsers.indexOf(x.selectedAnswer) > -1) {
        score += 1 / zacasniOdgovor.validAnwsers.length;
      }
    })

    req.body.poskus.score = Math.floor((score / maxscore) * 100);

    new Response({ 'id': req.body.poskus.id }).save(req.body.poskus).then((result) => {
      res.json(result);
    });
  });
});

module.exports = router;
