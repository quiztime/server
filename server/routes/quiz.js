var express = require('express');
var router = express.Router();
let config = require('../knexfile').development;
let knex = require('knex')(config);

let Quiz = require('../libs/models/bookshelf').Quiz;

/* GET all quizes containing string in name. */
router.get('/string/:id', function (req, res, next) {
  Quiz.query('where', 'quizName', 'LIKE', '%'+req.params.id+'%').fetchAll().then((result) => {
    res.json(result);
  });
});

/* GET all quizes responded to by a user. */
router.get('/responded/:id', function (req, res, next) {
  knex.select('*').from('quiz').havingExists(function () {
    this.select('*').from('response').whereRaw('quiz.id = response.quiz_id').whereRaw('response.user_id = ?', [req.params.id]);
  })
    .then((result) => {
      res.json(result);
    });
});

/* GET 20 newest quizes */
router.get('/', function (req, res, next) {
  Quiz.forge().orderBy("AvailableFrom", 'DESC').query((qb) => {
    qb.limit(20);
  }).fetchAll().then((result) => {
    res.json(result);
  }).catch(function (error) {
    console.log(error);
    res.send('An error occured');
  });
});


/* GET all quizes of a user. */
router.get('/:id', function (req, res, next) {
  Quiz.where('user_id', req.params.id).fetchAll().then((result) => {
    res.json(result);
  });
});


/* GET all quizes with name. */
router.get('/name/:id', function (req, res, next) {
  Quiz.where('quizName', req.params.id).fetchAll().then((result) => {
    res.json(result);
  });
});

/* GET whole quiz. */
router.get('/complete/:id', function (req, res, next) {
  Quiz.where('id', req.params.id).fetchAll({
    withRelated: ['question']
  }).then((result) => {
    res.json(result);
  });
});


//POST => add quiz
router.post('/', (req, res, next) => {
  new Quiz().save(req.body).then((result) => {
    res.json(result);
  });
});


// PUT => update quiz
router.put('/:id', function (req, res, next) {
  new Quiz({ 'id': req.params.id }).save(req.body).then((result) => {
    res.json(result);
  });
});


// DELETE => OK
router.delete('/:id', function (req, res) {
  new Quiz({ 'id': req.params.id }).destroy().then((result) => {
    res.json(result)
  });
});

//TO-DO: Delete all questions on deletion of quiz

module.exports = router;