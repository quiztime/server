var express = require('express');
var router = express.Router();
let config = require('../knexfile').development;
let knex = require('knex')(config);

let Answer = require('../libs/models/bookshelf').Answer;
let countAnswers = require('../libs/countAnswers')


/* GET all answers */
router.get('/', function (req, res, next) {
  Answer.fetchAll().then((result) => {
    res.json(result);
  });
});

/* GET count of different answers. */
router.get('/question/:id', function (req, res, next) {
  Answer.where('questions_id', req.params.id).fetchAll().then((result) => {
    res.json(countAnswers.countAnswers(result.toJSON(), 'selectedAnswer'));
  });
});

/* GET all answers of a question. */
router.get('/question/:id', function (req, res, next) {
  Answer.where('questions_id', req.params.id).fetchAll().then((result) => {
    res.json(result);
  });
});

/* GET all answers of a response. */
router.get('/response/:id', function (req, res, next) {
  Answer.where('response_id', req.params.id).fetchAll().then((result) => {
    res.json(result);
  });
});


//POST => add answer
router.post('/', (req, res, next) => {
  new Answer().save(req.body).then((result) => {
    res.json(result);
  });
});


// PUT => update answer
router.put('/:id', function (req, res, next) {
  new Answer({ 'id': req.params.id }).save(req.body).then((result) => {
    res.json(result);
  });
});

// DELETE => OK
router.delete('/:id', function (req, res) {
  new Answer({ 'id': req.params.id }).destroy().then((result) => {
    res.json(result)
  });
});

router.post('/multiple', (req, res, next) => {
  knex('answer').insert(req.body).then((a) =>{ 
    console.log(a);
    res.json(a);
  });
});



module.exports = router;
