var express = require('express');
var router = express.Router();

let Question = require('../libs/models/bookshelf').Question;


/* GET all questions */
router.get('/', function (req, res, next) {
  Question.fetchAll().then((result) => {
    res.json(result);
  });
});

/* GET all questions of a quiz */
router.get('/:id', function (req, res, next) {
  Question.where('quiz_id', req.params.id).fetchAll().then((result) => {
    res.json(result);
  });
});


/* GET number of questions in a quiz */
router.get('/count/:id', function (req, res, next) {
  Question.where('quiz_id', req.params.id).count().then((result) => {
    res.json(result);
  });
});


//POST => add question
router.post('/', (req, res, next) => {
  new Question().save(req.body).then((result) => {
    res.json(result);
  });
});


// PUT => update question
router.put('/:id', function(req, res, next){
    new Question({'id' : req.params.id}).save(req.body).then((result) => {
        res.json(result);
    });
});


// DELETE => OK
router.delete('/:id', function(req, res){
    new Question({'id' : req.params.id}).destroy().then((result) => {
        res.json(result)
    });
});


module.exports = router;
