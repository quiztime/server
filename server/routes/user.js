var express = require('express');
var router = express.Router();
var md5 = require('md5');

let User = require('../libs/models/bookshelf').User;


/* GET users listing. Use: ?dataField=something&dataField2=something ...*/
router.get('/', function (req, res, next) {
  User.where(req.query).fetchAll().then((result) => {
    res.json(result);
  });
});

// POST user/pass -> return user
router.post('/authenticate', function (req, res, next) {
  User.where('username', req.body.username).where('password', req.body.password).fetch({
    columns: ['id', 'username']
  }).then((result) => {
    res.json(result);
  }).catch(function (error) {
    console.log(error);
    res.send('An error occured');
  });
});


//POST => add user
router.post('/', (req, res, next) => {
  new User().save(req.body).then((result) => {
    res.json(result);
  });
});


// PUT => update user
router.put('/:id', function (req, res, next) {
  new User({ 'id': req.params.id }).save(req.body).then((result) => {
    res.json(result);
  });
});

// DELETE => OK
router.delete('/:id', function (req, res) {
  new User({ 'id': req.params.id }).destroy().then((result) => {
    res.json(result)
  });
});

//GET => vrne uporabnike z imenom name
router.get('/name/:id', function (req, res) {
  User.where("username", req.params.id).fetch({
    columns: ['username']
  }).then((result) => {
    res.json(result);
  });
});

//TO-DO: Delete all user data on deletion of user update all quezes to unavailable

module.exports = router;